FROM golang:1.8.0
MAINTAINER cos <info@cos.ovh>
RUN apt-get update
RUN apt-get install git
RUN apt-get autoremove
RUN curl https://glide.sh/get | sh
RUN apt-get clean
RUN git clone https://gitlab.com/cospoland/gifie /go/src/gifie
WORKDIR /go/src/gifie
RUN glide update && glide install && go build
CMD ["/go/src/gifie/gifie"]