package main

import (
	"flag"
	"fmt"
	"strings"
	"time"
	"database/sql"
	"os"
	"errors"
	"github.com/go-redis/redis"
	"github.com/valyala/fasthttp"
	"log"
	"bytes"
	_ "github.com/lib/pq"
	"github.com/bwmarrin/discordgo"
)

var magicTable = map[string]string{
    "\xff\xd8\xff":      ".jpeg",
    "\x89PNG\r\n\x1a\n": ".png",
    "GIF87a":            ".gif",
    "GIF89a":            ".gif",
    "RIFF":              ".webp",
}

// BEGIN http://stackoverflow.com/questions/25959386/how-to-check-if-a-file-is-a-valid-image
func mimeFromIncipit(incipit []byte) string {
    incipitStr := string(incipit)
    for magic, mime := range magicTable {
        if strings.HasPrefix(incipitStr, magic) {
            return mime
        }
    }

    return ""
}
// END

const Width = 128

const USE_FLAGS = false

var Database *sql.DB
var Token string
var DATABASE_URL string
var BotID string
var Prefix string
var ScaleGate string
var ScaleGateID string
var RedisHost string
var ScaleGateAuth string
var Debug bool
func init() {
    if !USE_FLAGS {
       Token = os.Getenv("Token")
       DATABASE_URL = os.Getenv("DATABASE_URL")
       Prefix = os.Getenv("Prefix")
       ScaleGate = os.Getenv("ScaleGate")
       ScaleGateID = os.Getenv("ScaleGateID")
       RedisHost = os.Getenv("REDIS_URL")
       Debug = os.Getenv("ENV") == "DEVELOPMENT"
       fmt.Printf("PREFIX: %s (%v)\n", os.Getenv("Prefix"), Prefix)
    } else {
    	flag.StringVar(&DATABASE_URL, "dburl", "", "Postgres database URL")
	    flag.StringVar(&Prefix, "prefix", "", "Command prefix")
	    flag.StringVar(&ScaleGate, "scalegate", "", "Sets the gate address which will be used as an alternative image source. This can be used to add watermarks or scale the image.")
	    flag.StringVar(&ScaleGateAuth, "scalegatetoken", "", "Sets the gate authorization token. Keep empty if NO_AUTH is true.")
	    flag.StringVar(&ScaleGateID, "scalegateid", "", "Sets the client ID to use with gate. Keep empty if NO_AUTH is true.")
	    flag.BoolVar(&Debug, "debug", false, "Enables much wider logs")
	    flag.Parse()
    }
}

var rclient *redis.Client

func main() {
    redisoptions, _ := redis.ParseURL(RedisHost)
	rclient = redis.NewClient(redisoptions)
    
	err := errors.New("avoid database being assigned to local var")
	Database, err = sql.Open("postgres", DATABASE_URL)
        if err != nil {
                fmt.Println("Error connecting to Postgres database,", err)
                return
        }
	r, err := Database.Query("CREATE TABLE IF NOT EXISTS gifieimg (name TEXT, guild TEXT, data BYTEA, ext TEXT)")
	if err != nil {
		panic(err)
	}
	fmt.Printf("Migration result: %v\n", r)
	dg, err := discordgo.New("Bot " + Token)
	if err != nil {
		fmt.Println("Error creating Discord session,", err)
		return
	}
	u, err := dg.User("@me")
	if err != nil {
		panic(err)
	}
	BotID = u.ID
	dg.AddHandler(messageCreate)
	dg.AddHandler(messageReacted)
	err = dg.Open()
	if err != nil {
		fmt.Println("Error opening connection,", err)
		return
	}
	fmt.Println("Bot is now running. Press CTRL-C to exit.")
	<-make(chan struct{})
	return
}

func messageReacted(s *discordgo.Session, m *discordgo.MessageReactionAdd) {
    if Debug {
        fmt.Printf("MREACT IN_%s\n", m.MessageID)
	    fmt.Printf("MREACT RECV %s\n", m.UserID);
    }
    r0, err := rclient.HExists(m.MessageID, "AuthorID").Result()
    r1, erra := rclient.HExists(m.MessageID, "FName").Result()
    if Debug {
        fmt.Printf("MREACT E/BOOLS_%v . %v | %v . %v\n", r0, err, r1, erra)
    }
    if err != nil || erra != nil || !r0 || !r1 {
        return
    }
    aid, err := rclient.HGet(m.MessageID, "AuthorID").Result()
	if err != nil {
        _ = s.ChannelMessageDelete(m.ChannelID, m.MessageID)
	    msgembed := discordgo.MessageEmbed {Title: "Encountered an error while processing your deletion request.", Color: 16711680}
	    _, _ = s.ChannelMessageSendEmbed(m.ChannelID, &msgembed)
        return
	}
	if Debug {
	    fmt.Printf("MREACT EXPECT %s\n", aid);
	}
    if m.UserID == aid {
	    if m.Emoji.Name == "rage" {
            _ = s.ChannelMessageDelete(m.ChannelID, m.MessageID)
	        msgembed := discordgo.MessageEmbed {Title: "Canceled the deletion request.", Color: 1692672}
	        _, _ = s.ChannelMessageSendEmbed(m.ChannelID, &msgembed)
            _, _ = rclient.HDel(m.MessageID, "FName").Result()
            _, _ = rclient.HDel(m.MessageID, "AuthorID").Result()
            return
        }
        fname, err := rclient.HGet(m.MessageID, "FName").Result()
	    if err != nil {
            _ = s.ChannelMessageDelete(m.ChannelID, m.MessageID)
	        msgembed := discordgo.MessageEmbed {Title: "Encountered an error while processing your deletion request.", Color: 16711680}
	        _, _ = s.ChannelMessageSendEmbed(m.ChannelID, &msgembed)
            return
	    }
	    if m.Emoji.Name == "grinning" {
            _ = s.ChannelMessageDelete(m.ChannelID, m.MessageID)
			guildid := m.ChannelID
			channelobj, _ := s.Channel(guildid)
			if !channelobj.IsPrivate {
			    guildid = channelobj.GuildID
			}
			_, err = Database.Query("DELETE FROM gifieimg WHERE guild = $1 AND name = $2", guildid, fname)
            _, _ = rclient.HDel(m.MessageID, "FName").Result()
            _, _ = rclient.HDel(m.MessageID, "AuthorID").Result()
	        if err != nil {
                _ = s.ChannelMessageDelete(m.ChannelID, m.MessageID)
		        msgembed := discordgo.MessageEmbed {Title: "Encountered an error while processing your deletion request.", Color: 16711680}
		        _, _ = s.ChannelMessageSendEmbed(m.ChannelID, &msgembed)
	        } else {
		        msgembed := discordgo.MessageEmbed {Title: fmt.Sprintf("Deleted %s.", fname), Color: 1692672}
		        _, _ = s.ChannelMessageSendEmbed(m.ChannelID, &msgembed)
	        }
            return
        }
    }
}

func messageCreate(s *discordgo.Session, m *discordgo.MessageCreate) {
	isCommand := strings.HasPrefix(m.Content, Prefix)
	if isCommand || Debug {
		fmt.Printf("%20s %20s %20s > %s\n", time.Now().Format(time.Stamp), m.ChannelID, m.Author.Username, m.Content)
	}
	if isCommand {
		mParams := strings.SplitN(m.Content[len(Prefix):len(m.Content)], " ", 2)
		if Debug {
		    fmt.Printf("COMMAND BREAKDOWN: %v %v\n", mParams, len(mParams))
		}
		if len(mParams) >= 2 && mParams[0] != "" {
		    if mParams[1] == "delete" {
	            if Debug {
	                fmt.Printf("REF CREATE (DELETE_REQUEST::%s) FOR %s ON %s\n", mParams[0], m.Author.ID, m.ID);
	            }
                se, err := rclient.HSet(m.ID, "AuthorID", m.Author.ID).Result()
                se1, errO := rclient.HSet(m.ID, "FName", mParams[0]).Result()
                if (!se || !se1) && Debug {
		            fmt.Printf("DEL_FAILCHECK SE&SE1 %v %v %v %v\n", se, se1, err, errO)
                }
		        if err == nil && errO == nil {
		            msgembed := discordgo.MessageEmbed {Title: "Confirm your decision", Description: fmt.Sprintf("Are you sure you want to delete %s? This will be unrevertable. React appropriately to my message.", mParams[0]), Color: 16774144}
		            msg, err := s.ChannelMessageSendEmbed(m.ChannelID, &msgembed)
                    if err == nil {
                        _ = s.MessageReactionAdd(m.ChannelID, msg.ID, "😀")
                        _ = s.MessageReactionAdd(m.ChannelID, msg.ID, "😡")
                    }
		        } else {
		            if Debug {
		                fmt.Printf("DEL_FAILINIT %v %v\n", err, errO)
		            }
		        }
		        return
		    }
			_ = s.MessageReactionAdd(m.ChannelID, m.ID, "🤔")
			req := fasthttp.AcquireRequest()
			if ScaleGate != "" {
				req.SetRequestURI(ScaleGate)
				req.SetBody([]byte(mParams[1]))
				req.Header.SetMethodBytes([]byte("POST"))
			} else {
				req.SetRequestURI(mParams[1])
			}
			if ScaleGateID != "" {
    			req.Header.Add("User-Agent", fmt.Sprintf("Gifie.Discord/1.2 - %s", ScaleGateID))
			} else {
    			req.Header.Add("User-Agent", "Gifie.Discord/1.2")
			}
			resp := fasthttp.AcquireResponse()
			client := &fasthttp.Client{}
			client.Do(req, resp)
			if resp.StatusCode() >= 200 && resp.StatusCode() <= 302 {
				respBody := resp.Body()
			    _ = s.MessageReactionRemove(m.ChannelID, m.ID, "🤔", BotID)
				if respBody == nil {
				    err := s.MessageReactionAdd(m.ChannelID, m.ID, "⁉️")
				    if err != nil {
				        s.ChannelMessageSend(m.Author.ID, "⁉️ Downloaded empty document.")
    		            if Debug {
    		                fmt.Println(err)
    		            }
				    }
					return
				}
				mime := mimeFromIncipit(respBody)
				if mime == "" {
				    err := s.MessageReactionAdd(m.ChannelID, m.ID, "❓")
				    if err != nil {
				        s.ChannelMessageSend(m.Author.ID, "❓ Unknown file type.")
    		            if Debug {
    		                fmt.Println(err)
    		            }
				    }
					return
				}
				guildid := m.ChannelID
				channelobj, _ := s.Channel(guildid)
				if !channelobj.IsPrivate {
				    guildid = channelobj.GuildID
				}
				_, err := Database.Query("INSERT INTO gifieimg (name, guild, data, ext) VALUES($1, $2, $3, $4)", mParams[0], guildid, respBody, mime)
				if err != nil {
					panic(err)
				}
    		    err = s.MessageReactionAdd(m.ChannelID, m.ID, "👌")
	    	    if err != nil {
			        s.ChannelMessageSend(m.Author.ID, "👌 Saved the image.")
    		        if Debug {
    		            fmt.Println(err)
    		        }
			    }
			} else {
			    err := s.MessageReactionAdd(m.ChannelID, m.ID, "⁉️")
			    if err != nil {
    		        s.ChannelMessageSend(m.Author.ID, "⁉️ Failed downloading the image.")
    		        if Debug {
    		            fmt.Println(err)
    		        }
			    }
			}
		} else if len(mParams) == 1 {
			if mParams[0] == "" {
				guildid := m.ChannelID
				channelobj, _ := s.Channel(guildid)
				if !channelobj.IsPrivate {
				    guildid = channelobj.GuildID
				}
				var name string
				rows, err := Database.Query("SELECT name FROM gifieimg WHERE guild = $1", guildid)
				if err != nil {
					log.Fatal(err)
				}
				msg := "```Attachments on this server:"
				for rows.Next() {
					err = rows.Scan(&name)
					if err != nil {
						log.Fatal(err)
					}
					msg = msg + " " + name + ","
				}
				dmchannel, err := s.UserChannelCreate(m.Author.ID)
				_, err = s.ChannelMessageSend(dmchannel.ID, msg + "```")
				if err != nil {
				    if Debug {
    					log.Fatal(err)
				    }
				}
			} else {
				var gifdata []byte
				var ext string
				guildid := m.ChannelID
				channelobj, _ := s.Channel(guildid)
				if !channelobj.IsPrivate {
				    guildid = channelobj.GuildID
				}
				err := Database.QueryRow("SELECT data, ext FROM gifieimg WHERE name = $1 AND guild = $2", mParams[0], guildid).Scan(&gifdata, &ext)
      			if err != nil {
      			    if Debug {
        				fmt.Println("⁉️ Error pulling the image data,", err)
      			    }
    			    err = s.MessageReactionAdd(m.ChannelID, m.ID, "⁉️")
			        if err != nil {
    		            if Debug {
    		                fmt.Println(err)
    		            }
			        }
					return
				}
				r := bytes.NewReader(gifdata)
				_, _ = s.ChannelFileSend(m.ChannelID, mParams[0] + ext, r)
			}
		}
	} else if strings.Contains(m.Content, Prefix) {
		mParams := strings.Split(m.Content, Prefix)
		mParams = mParams[1:len(mParams)]
		for _, gif := range mParams {
			gifname := strings.SplitN(gif, " ", 2)[0]
	        fmt.Printf("%20s %20s %20s => %s\n", time.Now().Format(time.Stamp), m.ChannelID, m.Author.Username, gifname)
			if gifname != "" {
				var gifdata []byte
				var ext string
				guildid := m.ChannelID
				channelobj, _ := s.Channel(guildid)
				if !channelobj.IsPrivate {
				    guildid = channelobj.GuildID
				}
				err := Database.QueryRow("SELECT data, ext FROM gifieimg WHERE name = $1 AND guild = $2", mParams[0], guildid).Scan(&gifdata, &ext)
      			if err != nil {
					if Debug {
					    fmt.Println("⁉️ Error pulling the image data,", err)
					}
					return
				}
				r := bytes.NewReader(gifdata)
				_, _ = s.ChannelFileSend(m.ChannelID, gifname + ext, r)
			}
		}
	}
}
